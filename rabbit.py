#program written to establish a connection with the rabbirmq server
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(host="localhost")) #connection is localhost
channel = connection.channel() #connect to localhost through channel
channel.queue_declare(queue='hello') #name of the queue established is 'hello'
        #message can never be sent directly to queue but has to sent to an exchange first
channel.basic_publish(exchange='', routing_key='hello', body="Hello World")
channel.basic_publish(exchange='', routing_key='hello', body="Next Message")
print("Sent the hello world message")
print("Sent next message")
connection.close() #message buffer should be flushed out and make sure message is delivered to rabbitmq server