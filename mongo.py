import pymongo
client = pymongo.MongoClient('localhost')
db = client['database_name'] #database declared but not created because it does not exist until it has a collection
# print(client.list_database_names()) #prints all the databases
collection = db['column5']
dict = {"name" : "John", "address" : "Leela Palace", "age" : "24"}
x = collection.insert_one(dict)
print(x.inserted_id) #insert_one returns the InsertOneResult object which has insert_id to hold the id of the inserted document
dict2 = [
    {"name" : "Jacob", "address" : "mac", "age" : "25"},
    {"name" : "Julius", "address" : "white orchid", "age" : "50"}
]
x = collection.insert_many(dict2)
print(x.inserted_ids)
dict3 = [
    {"_id" : 1, "name" : "samantha", "address" : "oberoi", "age" : "21"},
    {"_id" : 2, "name" : "ekka", "address" : "itc", "age" : "35"}
]

x = collection.insert_many(dict3)
print(x.inserted_ids)

x = collection.find_one() #finds the first document in the collection
print(x)

for n in collection.find():
    print(n) #all the documents in the collection

for n in collection.find({},{ "_id": 0, "name": 1, "address": 1, "age": 1 }):
  print(n)

query = {"address" : "mac"} #find the document with address mac
doc = collection.find(query)


