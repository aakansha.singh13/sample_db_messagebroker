import socket
TCP_IP = "192.168.1.2"
TCP_PORT = 9001
BUFFER_SIZE = 1024
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
with open("received file","wb") as f:
    print("File opened")
    while True:
        data = s.recv(BUFFER_SIZE)
        print('Data = %s', (data))
        if not data:
            f.close()
            print("File Closed")
            break
print("Successfully got the file")
s.close()
print("Connection closed")
