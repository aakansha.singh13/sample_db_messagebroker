import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))  # connection is localhost
channel = connection.channel()  # connect to localhost through channel
channel.queue_declare(queue='hello')
def callback(ch, method, properties, body):
     print("Received  %r" % body)
channel.basic_consume(queue='hello', auto_ack=True, on_message_callback=callback)
print('Waiting for messages. To exit press CTRL+C')
channel.start_consuming()