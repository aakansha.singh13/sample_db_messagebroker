from cassandra import ConsistencyLevel
from cassandra.cluster import Cluster
from cassandra.query import SimpleStatement



KEYSPACE = "myKeyspace1" #namespace that defines data replication on nodes

cluster = Cluster(['127.0.0.1']) #cluster has multiple nodes where there is one keyspace per node

session = cluster.connect() #session is connection to a cassandra cluster

session.execute("""CREATE KEYSPACE IF NOT EXISTS myKeyspace1 WITH replication = {'class' : 'SimpleStrategy', 'replication_factor' : '2' };""" ) #create keyspace

session.set_keyspace("myKeyspace1") #set keyspace for the node

session.execute("""USE myKeyspace1;""")

session.execute("""CREATE TABLE IF NOT EXISTS myTable (theKey text, col1 text, col2 text, col3 text, PRIMARY KEY(theKey, col1));""") #create table

query = SimpleStatement("""INSERT INTO myTable (theKey, col1, col2, col3) VALUES (%(key)s, %(col1)s, %(col2)s, %(col3)s);""")

prepared = session.prepare("""INSERT INTO myTable (theKey, col1, col2, col3) VALUES (?, ?, ?, ?);""")


for i in range(10):

    session.execute(query, dict(key = "key%d" % i, col1 = "a%d" % i, col2 = "b%d" % i, col3 = "c%d" % i))
    session.execute(prepared, ("key%d" % i, "a%d" % i, "b%d" % i, "c%d" % i))

future = session.execute("""SELECT * FROM myTable""")

try:
    rows = future.result()
    print(rows)
except Exception:
    print("Error reading")



