import redis

redis_host = "localhost"
redis_port = 6379
redis_password = ""

def hello_world_redis():
    try:
        red = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password) #Create client
        red.set("message","Hello Redis!!")
        msg = red.get("message")
        print(msg)

    except Exception as e:
        print(e)

def hash_redis():
    try:
        red = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password) #create client
        red.hset("HashName","1","one") #hash with hashname, key and value
        red.hset("HashName","2","two")
        red.hset("HashName","3","three")
        red.hset("HashName","4","four")
        red.hset("HashName","5","five")
        print("Value for key 1 is ")
        print(red.hget("HashName","1")) #get the value with HashName and the key
        print("All the keys in redis hash are")
        print(red.hkeys("HashName")) #get all the keys in the hash with HashName
        print("All the values in hash are")
        print(red.hvals("HashName")) #get all the values in hash HashName
        print("Everything in hash")
        print(red.hgetall("HashName")) #get all values and keys in hash HashName
    except Exception as e:
        print(e)
def list_redis(): #lists in redis implemented as linked list
    try:
        redCli = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password) #create redis client
        redCli.lpush("ListName","One") #create list and push to head
        redCli.lpush("ListName","Two")
        redCli.lpush("ListName","Three")
        redCli.lpush("ListName","Four")
        redCli.lpush("ListName","Five")
        redCli.lpush("ListName","Six","Seven","Eight","Nine","Ten") #insert multiple values to list
        print("Popped elements from list")
        while(redCli.llen("ListName")!=0): #llen to find length of list
            print(redCli.lpop("ListName")) #pops element from list-left
        ListName2 = "ListName2"
        redCli.rpush(ListName2,1) #create list and push to tail
        redCli.rpush(ListName2,2)
        redCli.rpush(ListName2,3)
        redCli.rpush(ListName2,4)
        redCli.rpush(ListName2,5)
        redCli.rpush(ListName2,6,7,8,9,10) #insert mulitple valiues to tail
        print("Redis list before modification")
        for i in range(0, redCli.llen(ListName2)):
            print(redCli.lindex(ListName2, i)) #print redis list through the index values

        for i in range(0, redCli.llen(ListName2)):
            print(redCli.lset(ListName2, i, i*2)) #modify redis-list contents to multiplied by 2
        print("Popped elements from list")
        while(redCli.llen(ListName2) != 0):
            print(redCli.rpop(ListName2)) #pops element from list-right / head, modified redis list contents

    except Exception as e:
        print(e)

def sortedSet_redis(): #sorted set is like hash map where elements are sorted based on key, set where elements are always sorted based on a collection of scores
    try:
        redCli = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password) #create redis client
        SortedSet = "SortedSetName1"
        score = 20
        name = "name1"
        redCli.zadd(SortedSet, {name: score}) #add a name to the redis sorted set with their score
        score = "29"
        name = "name2"
        redCli.zadd(SortedSet, {name: score})
        print("Contents of sorted set in ascending order")
        print(redCli.zrange(SortedSet, 0, -1)) #retirve contents of sorted set in an order
        print("Contents of sorted set in descending order")
        print(redCli.zrange(SortedSet, 0, -1, desc=True))
        print("Contents of Sorted set with scores")
        print(redCli.zrange(SortedSet, 0, -1, withscores=True))
        print("Cardinality of redis sorted set is")
        print(redCli.zcard(SortedSet)) #retrieve cardinality od sorted set
        print("Count of elements with a score vote between 15 to 25")
        print(redCli.zcount(SortedSet, 15, 25)) #score between 15 to 25
        print("Find rank/score of an element in sorted set") #find score of name3
        elementValue = "name2"
        print("Score of the element with value '{}' is" .format(elementValue))
        print(redCli.zscore(SortedSet, "name2"))
        print("Retrieve elements based on range of scores") #retrieve elements based on range of scores with scores
        print(redCli.zrangebyscore(SortedSet, min=0, max=30, withscores=True))
        print("Retrieve elements based on range of scores in reverse order") #retrieve elements based on range of scores in reverse order with scores
        print(redCli.zrevrangebyscore(SortedSet, min=0, max=30, withscores=True))
        print("Remove elements from sorted set")
        print(redCli.zrem(SortedSet,"name1")) #remove element name1 with scores from the list

        score = 51
        name = "name3"
        redCli.zadd(SortedSet, {name: score})
        score = 59
        name = "name4"
        redCli.zadd(SortedSet, {name: score})
        print("Remove element by score") #remove all elements with score between 50 and 60
        print(redCli.zremrangebyscore(SortedSet, 55, 60))
    except Exception as e:
        print(e)





if __name__ == "__main__":
    #hello_world_redis()
    #hash_redis()
    #list_redis()
    sortedSet_redis()